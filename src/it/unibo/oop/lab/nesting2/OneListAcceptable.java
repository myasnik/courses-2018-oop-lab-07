package it.unibo.oop.lab.nesting2;

import java.util.LinkedList;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {

	final List<T> list = new LinkedList<T>();
	
	public OneListAcceptable(java.util.List<T> list) {
		// TODO Auto-generated constructor stub
		Acceptor<T> acceptor = this.acceptor();
		try {
			if(this.list.size() != list.size()) {
				acceptor.end();
			}
			
		}
		
	}

	@Override
	public Acceptor<T> acceptor() {
		Acceptor<T> acceptor = new Acceptor<T>() {
				
			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				if(newElement != list) {
					throw new ElementNotAcceptedException(list);
				}
			}

			@Override
			public void end() throws EndNotAcceptedException {
				throw new EndNotAcceptedException();
			}
		};
		return acceptor;
		
	}

}
